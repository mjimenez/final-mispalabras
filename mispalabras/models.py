import datetime

from django.db import models
from django.contrib.auth.models import User

class Usuario(models.Model):
    usuario = models.ForeignKey(User, primary_key=True, on_delete=models.CASCADE, default='')
    '''username = models.CharField(max_length=20,primary_key=True)
    #email = models.EmailField(null=True)
    password = models.CharField(max_length=20)'''


class Word(models.Model):
    word = models.TextField(primary_key=True)
    defi = models.TextField()
    img = models.TextField(default='')
    contador = models.IntegerField(default=0)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE, blank=True, null=True)
    date = models.DateTimeField()

class Comment(models.Model):
    comentario = models.TextField(max_length=256)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE, blank=True, null=True)
    palabra = models.ForeignKey(Word, on_delete=models.CASCADE, blank=True, null=True)
    date = models.DateTimeField()

class Voto(models.Model):
    votado = models.BooleanField(default=False)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE, blank=True, null=True)
    palabra = models.ForeignKey(Word, on_delete=models.CASCADE, blank=True, null=True)

class Info(models.Model):
    web = models.TextField(default ='')
    defi = models.TextField(default ='')
    img = models.TextField(default ='')
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE, blank=True, null=True)
    palabra = models.ForeignKey(Word, on_delete=models.CASCADE, blank=True, null=True)
    date = models.DateTimeField()

class Enlace(models.Model):
    url = models.TextField(default ='')
    defi = models.TextField(default ='')
    img = models.TextField(default ='')
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE, blank=True, null=True)
    palabra = models.ForeignKey(Word, on_delete=models.CASCADE, blank=True, null=True)
    date = models.DateTimeField()
