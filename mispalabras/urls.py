from django.urls import path
from . import views

urlpatterns = [
    path('logout/', views.user_logout, name='logout'),
    path('register/', views.register, name='register'),
    path('login/', views.user_login, name='login'),
    path('mipagina/', views.mipagina, name='mipagina'),
    path('ayuda/', views.ayuda, name='ayuda'),
    path('<word>', views.palabra, name='palabra'),
    path('', views.index, name='index'),
]
