from .models import Word, Comment, Voto, Usuario, Info, Enlace
from .forms import ContentForm, TextForm
from urllib.error import HTTPError
from xml.dom.minidom import parse
import urllib.request
from urllib.parse import quote
import json
from html.parser import HTMLParser
from django.utils import timezone


def gest_guardar(request,word,defi):
    try:
        p = Word.objects.get(word=word)
        guarda = None
    except Word.DoesNotExist:
        if 'guarda' in request.POST:
            p = Word(word=word)
            p.defi = defi[0]
            p.img = defi[1]
            p.usuario = Usuario.objects.get(usuario=request.user)
            p.date = timezone.now()
            p.save()
            guarda = None
        else:
            guarda = 'Guardar palabra'
    return guarda



def gest_voto(request, word):
    try:
        p = Word.objects.get(word=word)
        if request.method == 'POST' and 'voto' in request.POST:
            try:
                v = Voto.objects.get(palabra=word, usuario=Usuario.objects.get(usuario=request.user))
                if v.votado:
                    voto = 'Votar palabra'
                    v.votado = False
                else:
                    voto = 'Quitar voto'
                    v.votado = True
            except Voto.DoesNotExist:
                voto = 'Quitar voto'
                v = Voto()
                v.votado = True
                v.palabra = p
                v.usuario = Usuario.objects.get(usuario=request.user)
                v.date = timezone.now()
            v.save()
            p.contador = len(Voto.objects.filter(votado=True,palabra=word))
            p.save()
        else:
            try:
                v = Voto.objects.get(palabra=word, usuario=Usuario.objects.get(usuario=request.user))
                if v.votado:
                    voto = 'Quitar voto'
                else:
                    voto = 'Votar palabra'
            except Voto.DoesNotExist:
                voto = 'Votar palabra'
    except Word.DoesNotExist:
        voto=''
    return voto



def gest_comentario(request, word):
    if 'comment' in request.POST:
        p = Word.objects.get(word=word)
        form = ContentForm(request.POST)
        if form.is_valid():
            c =Comment(comentario=form.cleaned_data['content'])
            c.palabra = p
            c.usuario = Usuario.objects.get(usuario=request.user)
            c.date = timezone.now()
            c.save()


def get_date(elemento):
    return elemento[1]



def wiki(request, word):
    try:
        texto = "https://es.wikipedia.org/w/api.php?action=query&format=xml&titles="+str(quote(word))+"&prop=extracts&exintro&explaintext"
        xml = parse(urllib.request.urlopen(texto))
        extract = (xml.getElementsByTagName('extract')).item(0)
        defi = extract.firstChild.nodeValue
    except AttributeError:
        defi = ('No se ha encontrado definición')

    try:
        imagen = "https://es.wikipedia.org/w/api.php?action=query&titles="+str(quote(word))+"&prop=pageimages&format=json&pithumbsize=200"
        json_str = urllib.request.urlopen(imagen)
        json_doc = json_str.read().decode(encoding="ISO-8859-1")
        img = json.loads(json_doc)
        for key in img['query']['pages'].keys():
            id = key
        url_imagen = img['query']['pages'][id]['thumbnail']['source']
        #open_img = urlretrieve(url_imagen,'mispalabras/templates/pages/imagen.jpg')
    except KeyError:
        url_imagen = ''
    return (defi,url_imagen)



class ParserDefi(HTMLParser):
    def __init__(self):
        super().__init__()
        self.defi = ''
        self.atr =[]
        self.select = False
        self.val = ''
        self.sel = False
    def handle_starttag(self, tag, attrs):
        if tag == 'meta':
            self.atr.append(('Final','Inicio'))
            for atr in attrs:
                self.atr.append(atr)
            for at in self.atr:
                if at[0] == 'Final' and at[1] == 'Inicio':
                    self.val = ''
                    self.select = False
                    self.sel = False
                if self.select:
                    if at[0] == 'content':
                        self.defi = at[1]
                        self.select = False
                        break
                if at[0] == 'property' and at[1] == 'og:description':
                    self.select = True
                    if self.sel:
                        self.defi = self.val
                        break
                if at[0] == 'content':
                    self.sel = True
                    self.val = at[1]


class ParserImg(HTMLParser):
    def __init__(self):
        super().__init__()
        self.img = ''
        self.atr =[]
        self.sel_img = False
        self.val = ''
        self.sel = False
    def handle_starttag(self, tag, attrs):
        if tag == 'meta':
            self.atr.append(('Final', 'Inicio'))
            for atr in attrs:
                self.atr.append(atr)
            for at in self.atr:
                if at[0] == 'Final' and at[1] == 'Inicio':
                    self.val = ''
                    self.sel_img = False
                    self.sel = False
                if self.sel_img:
                    if at[0] == 'content':
                        self.img = at[1]
                        self.sel_img = False
                        break
                if at[0] == 'property' and at[1] == 'og:image':
                    self.sel_img = True
                    if self.sel:
                        self.img = self.val
                        break
                if at[0] == 'content':
                    self.sel = True
                    self.val = at[1]


def rae(request,word):
    if request.method == 'POST' and 'rae' in request.POST:
        url = 'https://dle.rae.es/'+str(quote(word))
        user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
        headers = {'User-Agent': user_agent}
        req = urllib.request.Request(url, headers=headers)
        parserdefi = ParserDefi()
        parserimg = ParserImg()
        with urllib.request.urlopen(req) as response:
            html = response.read().decode('utf8')
            parserdefi.feed(html)
            parserimg.feed(html)
            definicion = [parserdefi.defi,parserimg.img]
        r = Info(web='rae')
        r.defi = definicion[0]
        r.palabra = Word.objects.get(word=word)
        r.img = definicion[1]
        r.usuario = Usuario.objects.get(usuario=request.user)
        r.date = timezone.now()
        r.save()
    else:
        definicion = [None,None]
    return definicion



def flickr(request,word):
    if request.method == 'POST' and 'flickr' in request.POST:
        try:
            url = "https://www.flickr.com/services/feeds/photos_public.gne?tags=" + str(quote(word))
            xml = parse(urllib.request.urlopen(url))
            feed = xml.getElementsByTagName('feed')
            entry = feed[0].getElementsByTagName('entry')
            link = entry[0].getElementsByTagName('link')
            for tag in link:
                atributo = tag.getAttribute('rel')
                if atributo == 'enclosure':
                    href = tag.getAttribute('href')

        except AttributeError:
            href = ''
        except IndexError:
            href = ''
        i = Info(web='flickr')
        i.img = href
        i.usuario = Usuario.objects.get(usuario=request.user)
        i.palabra = Word.objects.get(word=word)
        i.date = timezone.now()
        i.save()
    else:
        try:
            href = Info.objects.get(web='flickr',palabra=word).img
        except Info.DoesNotExist:
            href = None
    return href



def apimeme(request,word):
    if request.method == 'POST' and 'apimeme' in request.POST:
        form = TextForm(request.POST)
        if form.is_valid():
            texto = form.cleaned_data['text']
            option = form.cleaned_data['option']
            if option == '1':
                img = "http://apimeme.com/meme?meme=Advice-Yoda&top="+ str(quote(word))+"&bottom=" + str(quote(texto))
            elif option == '2':
                img = "http://apimeme.com/meme?meme=Ancient-Aliens&top="+ str(quote(word))+"&bottom=" + str(quote(texto))
            elif option == '3':
                img = "http://apimeme.com/meme?meme=Afraid-To-Ask-Andy&top="+ str(quote(word))+"&bottom=" + str(quote(texto))

        i = Info(web='apimeme')
        i.img = img
        i.usuario = Usuario.objects.get(usuario=request.user)
        i.palabra = Word.objects.get(word=word)
        i.date = timezone.now()
        i.save()
    else:
        try:
            img = Info.objects.get(web='apimeme',palabra=word).img
        except Info.DoesNotExist:
            img = None
    return img



def gest_enlace(request,word):
    if request.method == 'POST' and 'btnenlace' in request.POST:
        try:
            form = request.POST['enlace']
            #if form.is_valid():
            #url = form.cleaned_data['enlace']
            user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
            headers = {'User-Agent': user_agent}
            req = urllib.request.Request(form, headers=headers)
            response = urllib.request.urlopen(req)
            html = response.read().decode('utf8')
            parserdefi = ParserDefi()
            parserimg = ParserImg()
            parserdefi.feed(html)
            parserimg.feed(html)
            definicion = [parserdefi.defi, parserimg.img]
            r = Enlace(url=form)
            r.defi = definicion[0]
            r.palabra = Word.objects.get(word=word)
            r.img = definicion[1]
            r.usuario = Usuario.objects.get(usuario=request.user)
            r.date = timezone.now()
            r.save()
        except UnicodeError:
            r = Enlace(url=form)
            r.defi = ''
            r.palabra = Word.objects.get(word=word)
            r.img = ''
            r.usuario = Usuario.objects.get(usuario=request.user)
            r.date = timezone.now()
            r.save()
        except HTTPError:
            form = request.POST['enlace']
            response = urllib.request.urlopen(form)
            html = response.read().decode('utf8')
            parserdefi = ParserDefi()
            parserimg = ParserImg()
            parserdefi.feed(html)
            parserimg.feed(html)
            definicion = [parserdefi.defi, parserimg.img]
            r = Enlace(url=form)
            r.defi = definicion[0]
            r.palabra = Word.objects.get(word=word)
            r.img = definicion[1]
            r.usuario = Usuario.objects.get(usuario=request.user)
            r.date = timezone.now()
            r.save()
    try:
        info = Enlace.objects.filter(palabra=word)
    except Enlace.DoesNotExist:
        info = None
    return info



class ParserTraduccion(HTMLParser):
    def __init__(self):
        super().__init__()
        self.defi = []
        self.atr =[]
        self.select = False
    def handle_starttag(self, tag, attrs):
        if tag == 'td':
            for atr in attrs:
                self.atr.append(atr)
            for at in self.atr:
                if at[0] == 'class' and at[1] == 'ToWrd':
                    self.select = True
                else:
                    self.select = False
    def handle_data(self, data):
        if self.select:
            self.defi.append(data)


def gest_traduccion(request,word):
    if request.method == 'POST' and 'traduccion' in request.POST:
        url = 'https://www.wordreference.com/es/en/translation.asp?spen=' + str(quote(word))
        user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
        headers = {'User-Agent': user_agent}
        req = urllib.request.Request(url, headers=headers)
        parsertradu = ParserTraduccion()
        with urllib.request.urlopen(req) as response:
            html = response.read().decode('utf8')
            parsertradu.feed(html)
            try:
                traduccion = (parsertradu.defi[2]).split(sep=',')[0]
            except IndexError:
                traduccion = ''
        r = Info(web='wordreference')
        r.defi = traduccion
        r.palabra = Word.objects.get(word=word)
        r.img = ''
        r.usuario = Usuario.objects.get(usuario=request.user)
        r.date = timezone.now()
        r.save()
    else:
        traduccion = None
    return traduccion



def get_rima(request,word):
    if request.method == 'POST' and 'rima' in request.POST:
        url = 'https://www.wordreference.com/es/en/translation.asp?spen=' + str(quote(word))
        user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
        headers = {'User-Agent': user_agent}
        req = urllib.request.Request(url, headers=headers)
        parsertradu = ParserTraduccion()
        with urllib.request.urlopen(req) as response:
            html = response.read().decode('utf8')
            parsertradu.feed(html)
            try:
                traduccion = (parsertradu.defi[2]).split(sep=',')[0]
            except IndexError:
                traduccion = ''
        web = "https://rhymebrain.com/talk?function=getRhymes&word=" + str(quote(traduccion))
        json_str = urllib.request.urlopen(web)
        json_doc = json_str.read().decode(encoding="ISO-8859-1")
        codigo = json.loads(json_doc)
        rima = []
        for key in codigo:
            rima.append(key['word'])
        palabras = rima[0:4]
        r = Info(web='rhymebrain')
        r.defi = palabras
        r.palabra = Word.objects.get(word=word)
        r.img = ''
        r.usuario = Usuario.objects.get(usuario=request.user)
        r.date = timezone.now()
        r.save()
    else:
        palabras = None
    return palabras