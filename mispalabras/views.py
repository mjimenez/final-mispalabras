from django.contrib.auth import logout, login, authenticate
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from .models import Word, Comment, Voto, Usuario, Info, Enlace
from .forms import ContentForm, UserForm, TextForm, UrlForm
from django.contrib.auth.forms import AuthenticationForm
from django.core.paginator import Paginator
import random
from . import gestiones


@csrf_exempt
def index(request):
    lista = Word.objects.all().order_by('-date')
    listvoto = Word.objects.all().order_by('-contador')[0:5]
    try:
        palabra = [len(lista),random.choice(lista).word]
    except IndexError:
        palabra = [0,None]

    paginator = Paginator(lista, 5)

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    if request.method == 'GET' and 'format' in request.GET:
        formato = request.GET.get('format')
        if formato == 'xml':
            response = render(request, 'pages/contenidos.xml', {'Lista': lista}, content_type='text/xml')
        if formato == 'json':
            response = render(request, 'pages/contenidos.json', {'Lista': lista}, content_type='text/json')
        return response

    if request.method == 'GET' or request.method == 'POST':
        forme = AuthenticationForm()
        form = request.POST.get('content')
        if form is not None:
            return HttpResponseRedirect('/'+str(form))
        else:
            response = render(request, 'pages/word_form.html', {'Lista': lista, 'login': forme, 'word': '',
                                                                'listvoto': listvoto, 'numpalabras': palabra[0],
                                                                'palabra': palabra[1], 'page_obj': page_obj})
        return HttpResponse(response)



def palabra(request,word):
    word = word.lower()

    try:
        if request.method == 'GET' and 'format' in request.GET:
            formato = request.GET.get('format')
            pala = Word.objects.get(word=word)
            try:
                com = Comment.objects.filter(palabra=word)
            except Comment.DoesNotExist:
                com =''
            try:
                vot = Voto.objects.filter(palabra=word)
            except Voto.DoesNotExist:
                vot = ''
            try:
                inf = Info.objects.filter(palabra=word)
            except Info.DoesNotExist:
                inf=''
            try:
                enla = Enlace.objects.filter(palabra=word)
            except Enlace.DoesNotExist:
                enla =''
            if formato == 'xml':
                response = render(request, 'pages/palabra.xml', {'pala': pala, 'com':com, 'vot':vot, 'inf': inf, 'enla': enla},
                                  content_type='text/xml')
            if formato == 'json':
                response = render(request, 'pages/palabra.json', {'pala': pala, 'com':com, 'vot':vot, 'inf': inf, 'enla': enla},
                                  content_type='text/json')
            return response

        if request.method == 'GET' or request.method == 'POST':
            try:
                p = Word.objects.get(word=word)
                defi = (p.defi,p.img)
                guarda = None
                gestiones.gest_comentario(request, word)
            except Word.DoesNotExist:
                defi = gestiones.wiki(request,word)
                if request.method == 'GET':
                    guarda = 'Guardar palabra'
                elif request.method == 'POST':
                    guarda = gestiones.gest_guardar(request, word, defi)
            imgfli = gestiones.flickr(request, word)
            comentarios = Comment.objects.filter(palabra=word)
            voto = gestiones.gest_voto(request, word)
            try:
                infrae = Info.objects.get(web='rae',palabra=word)
                drae = [infrae.defi,infrae.img]
            except Info.DoesNotExist:
                drae = gestiones.rae(request,word)
            try:
                tradu = Info.objects.get(web='wordreference',palabra=word)
                traducida = tradu.defi
            except Info.DoesNotExist:
                traducida = gestiones.gest_traduccion(request,word)
            try:
                rima_info = Info.objects.get(web='rhymebrain',palabra=word)
                rimas = rima_info.defi[1:-1]
            except Info.DoesNotExist:
                rimas = gestiones.get_rima(request,word)
            try:
                cuenta = Word.objects.get(word=word)
                veces = cuenta.contador
            except  Word.DoesNotExist:
                veces = 0
            comment = ContentForm()
            enlace = UrlForm()
            tarjeta = gestiones.gest_enlace(request,word)
            lista = Word.objects.all().order_by('-date')
            listvoto = Word.objects.all().order_by('-contador')[0:5]
            try:
                palabra = [len(lista),random.choice(lista).word]
            except IndexError:
                palabra = [0,None]
            forme = UserForm()
            imgapi = gestiones.apimeme(request,word)
            texto = TextForm()
            content_re = render(request, 'pages/word.html', {'login': forme, 'word': word, 'defi': defi[0],
                                                             'imagen':defi[1], 'voto': voto, 'veces': veces,
                                                             'guarda': guarda, 'form': comment, 'comentarios': comentarios,
                                                             'drae': drae[0], 'draeimg': drae[1], 'numpalabras': palabra[0],
                                                             'palabra': palabra[1], 'imgfli': imgfli, 'texto':texto,
                                                             'imgapi': imgapi, 'enlace': enlace, 'tarjeta': tarjeta,
                                                             'listvoto': listvoto, 'traducida': traducida, 'rimas': rimas})
            response = HttpResponse(content_re)
    except TypeError:
        if request.method == 'GET' or request.method == 'POST':
            try:
                p = Word.objects.get(word=word)
                defi = (p.defi,p.img)
            except Word.DoesNotExist:
                defi = gestiones.wiki(request,word)

            lista = Word.objects.all().order_by('-date')
            listvoto = Word.objects.all().order_by('-contador')[0:5]
            try:
                palabra = [len(lista), random.choice(lista).word]
            except IndexError:
                palabra = [0, None]
            forme = UserForm()
            content_re = render(request, 'pages/word.html', {'word': word, 'defi': defi[0], 'imagen':defi[1],
                                                             'login': forme, 'numpalabras': palabra[0],
                                                             'palabra': palabra[1], 'listvoto': listvoto})
            response = HttpResponse(content_re)
    return (response)



def user_login(request):
    if request.method == "POST":
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return HttpResponseRedirect('/')
            else:
                form = AuthenticationForm()
        else:
            form = AuthenticationForm()
        message = 'Nombre de usuario o contraseña inválidos'
    else:
        form = AuthenticationForm()
        message = ''
    lista = Word.objects.all().order_by('-date')
    listvoto = Word.objects.all().order_by('-contador')[0:5]
    try:
        palabra = [len(lista), random.choice(lista).word]
    except IndexError:
        palabra = [0, None]

    context = {'message': message, 'login': form, 'listvoto': listvoto,
               'numpalabras': palabra[0], 'palabra': palabra[1]}
    return HttpResponse(render(request, 'registration/login.html', context))



def user_logout(request):
    logout(request)
    return HttpResponseRedirect("/")



def register(request):
    if request.method == "POST":
        form = UserForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.set_password(user.password)
            user.save()
            u = Usuario(usuario=user)
            u.save()
            login(request, user)
            return HttpResponseRedirect('/')
        form = UserForm()
        message = 'Nombre de usuario inválido: Solo se permiten 150 caracteres o menos. Letras, números y @/./+/-/_ only. ' \
                  'Puede que el nombre no esté disponible'
    else:
        form = UserForm()
        message =''
    lista = Word.objects.all().order_by('-date')
    listvoto = Word.objects.all().order_by('-contador')[0:5]
    try:
        palabra = [len(lista), random.choice(lista).word]
    except IndexError:
        palabra = [0, None]
    paginator = Paginator(lista, 5)

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {'register_form':form, 'message': message, 'listvoto': listvoto,
               'numpalabras': palabra[0], 'palabra': palabra[1], 'Lista': lista, 'page_obj': page_obj}
    return HttpResponse(render(request, 'pages/register.html', context))



def mipagina(request):
    if request.method == 'GET':
        lista = Word.objects.all().order_by('-date')
        listvoto = Word.objects.all().order_by('-contador')[0:5]
        try:
            palabra = [len(lista), random.choice(lista).word]
        except IndexError:
            palabra = [0, None]
        try:
            forme = AuthenticationForm()
            user = Usuario.objects.get(usuario=request.user)
            palabras = Word.objects.filter(usuario=user).order_by('-date')
            tp = Word()
            comentarios = Comment.objects.filter(usuario=user).order_by('-date')
            tc = Comment
            infos = Info.objects.filter(usuario=user).order_by('-date')
            ti = Info()
            enlaces = Enlace.objects.filter(usuario=user).order_by('-date')
            te = Enlace()
            todo = []
            for item in palabras:
                todo.append((item,item.date))
            for item in comentarios:
                todo.append((item,item.date))
            for item in infos:
                todo.append((item,item.date))
            for item in enlaces:
                todo.append((item,item.date))
            todo.sort(key=gestiones.get_date,reverse=True)
            final = []
            for item in todo:
                if (type(item[0])) == type(tp):
                    final.append((item[0],'1'))
                elif type(item[0]) == type(tc):
                    final.append((item[0],'2'))
                elif type(item[0]) == type(ti):
                    final.append((item[0],'3'))
                elif type(item[0]) == type(te):
                    final.append((item[0],'4'))
            return HttpResponse(render(request, 'pages/mipagina.html', {'login': forme, 'orden': final, 'numpalabras': palabra[0],
                                                                        'palabra': palabra[1], 'listvoto': listvoto}))
        except TypeError:
            forme = AuthenticationForm()
            lista = Word.objects.all().order_by('-date')
            listvoto = Word.objects.all().order_by('-contador')[0:5]
            try:
                palabra = [len(lista), random.choice(lista).word]
            except IndexError:
                palabra = [0, None]
            mensaje = 'Debes estar logueado para ver esta información'
            return HttpResponse(render(request, 'pages/mipagina.html', {'login': forme, 'mensaje': mensaje,
                                                                        'numpalabras': palabra[0], 'palabra': palabra[1],
                                                                        'listvoto': listvoto}))

def ayuda(request):
    if request.method == 'GET' or request.method == 'POST':
        lista = Word.objects.all().order_by('-date')
        listvoto = Word.objects.all().order_by('-contador')[0:5]
        try:
            palabra = [len(lista), random.choice(lista).word]
        except IndexError:
            palabra = [0, None]
        forme = AuthenticationForm()
        form = request.POST.get('content')
        if form is not None:
            return HttpResponseRedirect('/'+str(form))
        else:
            response = render(request, 'pages/ayuda.html', {'login': forme, 'listvoto': listvoto,
                                                                'numpalabras': palabra[0], 'palabra': palabra[1]})
        return HttpResponse(response)
