from django.test import TestCase
from django.test import Client
# Create your tests here.

class GetTests (TestCase):

    def test_postroot(self):
        """Check root resource"""

        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h2 align="center" class="cabecera">Lista de palabras</h2>', content)

    def test_postroot(self):
        """Check root resource"""

        c = Client()
        response = c.post('/')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h2 align="center" class="cabecera">Lista de palabras</h2>', content)

    def test_getpalabras(self):

        c = Client()
        response = c.get('/palabra')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h2 hidden>Palabra:</h2>', content)

    def test_postpalabras(self):

        c = Client()
        response = c.post('/palabra')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h2 hidden>Palabra:</h2>', content)

    def test_mipagina(self):

        c = Client()
        response = c.get('/mipagina/')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h2 align="center" class="cabecera">Tu historial en MisPalabras</h2>', content)

    def test_palabras(self):

        c = Client()
        response = c.get('/ayuda/')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h2 align="center" class="cabecera">Ayuda</h2>', content)

