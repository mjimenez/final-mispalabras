# Entrega practica
## Datos
* Nombre: Mariano Jiménez Ruiz
* Titulación: Grado en Ingeniería en Sistemas de TElecomunicación
* Despliegue (url): http://mjimenez.pythonanywhere.com/
* Video básico (url): https://youtu.be/pENM2aifyZU
* Video parte opcional (url): https://youtu.be/0_ANk8Xmh-Q

 
## Cuenta Admin Site
* admin/admin
## Cuentas usuarios
* juan/usuario
* carlos/usuario
* marta/usuario
* pepe/usuario

## Resumen parte obligatoria
En esta práctica, un usuario puede buscar información de una palabra y obtenerla de diferentes sitios de internet.
Así, como añadir enlaces o comentarios. La funcionalidad básica, es decir, ver que palabras se han guardado y poder
ver su descripción y su imagen, es posible sin haberse logueado. Para la funcionalidad total, es decir, obtener
información automática de otras webs, comentar
o añadir enlaces solo está permitido para usuarios logueados.

## Lista partes opcionales
* favicon.ico: añadido el favicon
* Contenidos XML de una palabra: En la página de cada palabra, una vez logueado y
guardada la palabra, hay un enlace que al hacer click en el muestra todos los contenidos en 
formato xml relacionados con esa palabra.
* Contenidos JSON de una palabra: En la página de cada palabra, una vez logueado y
guardada la palabra, hay un enlace que al hacer click en el muestra todos los contenidos en 
formato json relacionados con esa palabra.
* Revertir voto: Se puede quitar un voto, lo que hará que disminuyan el número de votos de una palabra
y si se quiere volver a votar esa palabra se puede.
* Información automática extra: He añadido un botón que al hacer click en él, obtiene y guarda la
traducción de una palabra. Se obtiene de la web: www.wordreference.com y la forma de obtenerla es
parseando un html con HTML.parser.
* Información automática extra: He añadido un botón que al hacer click en él, obtiene y guarda
palabras que riman con la palabra traducida al inglés. Se obtiene de la api https://rhymebrain.com/api.html
que ofrece un documento en formato json.